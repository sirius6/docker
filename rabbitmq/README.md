# RabbitMQ image

This image is based on `rabbitmq:3.8-management` image. It enables the following plugins:

* `rabbitmq_stomp` to enable STOMP. The image also expose the port 61613
* `rabbitmq_delayed_message_exchange` to enable delayed message delivery
